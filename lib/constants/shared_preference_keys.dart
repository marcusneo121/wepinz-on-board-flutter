class SharedPreferenceKeys {
  static const String userEmail = 'user_email';
  static const String userToken = 'user_token';
}
